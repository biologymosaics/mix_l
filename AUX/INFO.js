
/*
	Sorted

	{
		Version,
		Dependencies: []
	}
*/
var _Versions = {};
function Parse_Versions ({ Versions }) {
	return Object.keys (Versions).map (Version => {
		var Dependencies = Versions [ Version ].dependencies || {};

		return {
			Version,
			Dependencies: Parse_Dependencies ({
				Dependencies
			})
		}
	}).sort ()
}

function Parse_Dependencies ({ Dependencies }) {
	return Object.keys (Dependencies).map (Key => {
		return {
			Name: Key,
			Semantic_Specifier: Dependencies [ Key ]
		}
	});
}

module.exports = function ({
	NAME
}) {
	var ADDRESS = `https://registry.npmjs.org/${ NAME }`;

	return new Promise (Resolve => {
		require ('axios')
		.get (ADDRESS)
		.then (function (response) {
			var _Versions = require ("lodash/get") (response, "data.versions", {});
			var Latest_Version = response.data ['dist-tags'].latest;

			var Info = _Versions [ Latest_Version ];
			var Dependencies = Info.dependencies || {};

			var Versions = Parse_Versions ({
				Versions: require ("lodash/get") (response, "data.versions", {})
			});


			// console.dir (_Versions, {
			// 	depth: 10
			// });

			var Parsed_Dependencies = Parse_Dependencies ({ Dependencies });

			var Latest = {
				Version: Latest_Version,
				Dependencies: Parsed_Dependencies
			};

			Resolve ({
				Dependencies: Parsed_Dependencies,
				Latest,
				Versions
			});
		})
		.catch (function (error) {
			console.error (error);
			process.exit (2);
		});
	});
}
