

module.exports = function () {
	var Result = {};

	Object
	.entries (INSTALLATIONS)
	.sort ()
	.forEach (entry => {
		Result [ entry [ 0 ] ] = entry [ 1 ]
	});

	for (let NAME in Result) {
		const VERSIONS = Result [ NAME ];

		for (let VERSION in VERSIONS) {
			Result [ NAME ] [ VERSION ].DEPENDENTS.sort ((A, B) => {
				if (A.NAME > B.NAME) return 1;
				if (A.NAME < B.NAME) return -1;

				if (A.VERSION > B.VERSION) return 1;
				if (A.VERSION < B.VERSION) return -1;

				return 0;
			});
			Result [ NAME ] [ VERSION ].DEPENDENCIES.sort ((A, B) => {
				if (A.NAME > B.NAME) return 1;
				if (A.NAME < B.NAME) return -1;

				if (A.VERSION > B.VERSION) return 1;
				if (A.VERSION < B.VERSION) return -1;

				return 0;
			});
		}
	}

	return Result;
}
