

var HERE = process.cwd ();

module.exports = {
	HERE,
	MODULES: require ("path").resolve (
		HERE,
		"node_modules",
		"[MODULES]"
	)
}
