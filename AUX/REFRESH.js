
var HERE = require ("./SETTINGS").HERE;
var MODULES = require ("./SETTINGS").MODULES;

module.exports = async function () {
	var NODE_MODULES = require ("path").resolve (HERE, "node_modules");

	await new Promise ((Resolve) => {
		require ("fs").rmdir (NODE_MODULES, {
			recursive: true
		}, (Throw) => {
			if (Throw) {
				console.error (Throw);
				process.exit (2);
			}

			Resolve ();
		});
	});

	await new Promise ((Resolve) => {
		require ("fs").mkdir (NODE_MODULES, (Throw) => {
			if (Throw) {
				console.error (Throw);
				process.exit (2);
			}

			Resolve ();
		});
	});

	await new Promise ((Resolve) => {
		require ("fs").mkdir (MODULES, (Throw) => {
			if (Throw) {
				console.error (Throw);
				process.exit (2);
			}

			Resolve ();
		});
	});
}
