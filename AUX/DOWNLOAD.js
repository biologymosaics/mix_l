
// `https://registry.npmjs.org/${ NAME }/-/${ NAME }-${ VERSION }.tgz`
// `https://registry.npmjs.org/chalk/-/chalk-4.1.0.tgz`

// https://registry.npmjs.org/@mixes/a/-/a-0.0.1.tgz

// npm info @mixes/a

var TAR_STREAM = require ('tar-stream');
var pack = TAR_STREAM.pack()
var ZLIB = require ('zlib');

var RECORD = require ("./RECORD");

module.exports = function ({
	NAME,
	VERSION,
	MODULES
}) {
	var path;
	if (NAME.indexOf ("@") === 0) {
		const [ ORG, _NAME ] = NAME.split ("/");

		path = `/${ ORG }/${ _NAME }/-/${ _NAME }-${ VERSION }.tgz`;
	}
	else {
		path = `/${ NAME }/-/${ NAME }-${ VERSION }.tgz`;
	}

	return new Promise (Resolve => {
		const Retrieval = require ("https").request ({
			method: "GET",
			port: 443,
			host: 'registry.npmjs.org',
			path
		});

		Retrieval.on ('response', (Response) => {
			if (Response.statusCode !== 200) {
				console.error (`Error while retrieving ${ NAME }-${ VERSION }`);
				process.exit (2);
			}

			var extract = require ("tar-fs").extract (MODULES, {
				map: function (header) {
					// var Destination = `${ NAME }-${ VERSION }/` + header.name.substring (8);


					var Destination = `${ NAME }/${ VERSION }/` + header.name.substring (8);
					header.name = Destination;
					// console.log (Destination);

					return header;
				}
			});

			Response
			.pipe (ZLIB.createGunzip ())
			.pipe (extract)
			.on ('error', function (err) {
			    console.error ("error?????", err);
			})
			.on ('finish', function (err) {
				if (err) {
					console.error (err);
					// process.exit (2);
					return;
				}

			    RECORD ("/ Downloaded:", `${ NAME }/${ VERSION }`);
				Resolve ();
			});
		});

		Retrieval.end ();
	});
}
