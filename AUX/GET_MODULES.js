
var HERE = require ("./SETTINGS").HERE;
var MODULES = require ("./SETTINGS").MODULES;

async function getModules ({
	NAME = null,
	VERSION = null,

	DEPENDENCIES,
	DEPENDENTS = []
}) {
	return new Promise (Resolve => {
		require ("async/eachSeries") (
			DEPENDENCIES,
			function (Dependency, Conclude) {
				// Copy of dependents for each module in the loop
				var _DEPENDENTS = require ("lodash/cloneDeep") (DEPENDENTS);

				if (typeof NAME === "string") {
					_DEPENDENTS.push ({
						NAME,
						VERSION
					});
				}

				(async () => {
					await getModule ({
						NAME: Dependency.Name,
						SPECIFIER: Dependency.Semantic_Specifier,
						DEPENDENTS: _DEPENDENTS
					});

					Conclude ();
				}) ();
			},
			function (err) {
				if (err) {
					console.error (err);
					process.exit (2);
				}

				// console.log ("////////// FINISHED //////////", NAME, VERSION);

				Resolve ();
			}
		);
	});
}

function addDependency (DEPENDENT, DEPENDENCY) {
	var { NAME, VERSION } = DEPENDENT;

	INSTALLATIONS [ NAME ] [ VERSION ].DEPENDENCIES.push (DEPENDENCY)
}

async function getModule ({
	NAME,
	SPECIFIER = "*",
	DEPENDENTS = []
}) {
	console.log ("-----");
	// console.log (NAME, SPECIFIER, DEPENDENTS);

	var isOrg = false;
	if (NAME.indexOf ("@") === 0) {
		const [ ORG, _NAME ] = NAME.split ("/");
		isOrg = true;
	};

	var {
		Latest,
		Dependencies,
		Versions
	} = await require ("./INFO") ({
		NAME
	});

	var { ALREADY_INSTALLED, VERSION } = await require ("./GET_VERSION") ({
		NAME,
		SPECIFIER,
		Versions
	});

	if (DEPENDENTS.length >= 1) {
		addDependency (DEPENDENTS [ DEPENDENTS.length - 1 ], {
			NAME,
			VERSION,
			SPECIFIER
		});
	}

	if (!ALREADY_INSTALLED) {
		await require ("./DOWNLOAD") ({
			NAME,
			VERSION,
			MODULES
		});

		/*
			Add node_modules to DOWNLOADED module
		*/
		await new Promise (Resolve => {
			var MODULE_NODE_MODULES = require ("path").resolve (
				MODULES,
				NAME + "/" + VERSION,
				"node_modules"
			);

			require ("fs").mkdir (MODULE_NODE_MODULES, (Throw) => {
				if (Throw) {
					console.error (Throw);
					process.exit (2);
				}

				Resolve ();
			});
		});
	}

	/*
		Check if the current dependent that the module is being added because of
		was already added as a dependent.

		e.g.
			express
			body-parser
			iconv-lite
			safer-buffer

			express
			body-parser
			raw-body
			iconv-lite
			safer-buffer

			/// safer-buffer has already been linked to iconv-lite
	*/
	var ALREADY_LINKED = await new Promise (RESOLVE => {
		if (DEPENDENTS.length === 0) {
			RESOLVE (false);
			return;
		}

		var CURRENT_DEPENDENT = DEPENDENTS [ DEPENDENTS.length - 1 ];

		var ALL_DEPENDENTS = require ("lodash/get") (
			INSTALLATIONS,
			[ NAME, VERSION, "DEPENDENTS" ],
			[]
		);

		// console.log ({ ALL_DEPENDENTS });


		for (let A = 0; A < ALL_DEPENDENTS.length; A++) {
			const _DEPENDENT = ALL_DEPENDENTS [ A ];

			// console.log ({
			// 	_DEPENDENT,
			// 	CURRENT_DEPENDENT
			// });

			if (
				_DEPENDENT.NAME === CURRENT_DEPENDENT.NAME &&
				_DEPENDENT.VERSION === CURRENT_DEPENDENT.VERSION
			) {
				RESOLVE (true);
				return;
			}
		}

		RESOLVE (false);
	});

	/*
		if not already linked???????
	*/
	if (!ALREADY_LINKED) {
		await require ("./LINK") ({
			NAME,
			VERSION,
			MODULES,
			HERE,
			DEPENDENTS,
			isOrg
		});
	}

	if (INSTALLATIONS [ NAME ] === undefined) {
		INSTALLATIONS [ NAME ] = {}
	}

	if (INSTALLATIONS [ NAME ] [ VERSION ] === undefined) {
		INSTALLATIONS [ NAME ] [ VERSION ] = {
			SPECIFIER,
			DEPENDENTS: [],
			DEPENDENCIES: []
		}
	}

	if (DEPENDENTS.length >= 1) {
		INSTALLATIONS [ NAME ] [ VERSION ].DEPENDENTS.push (
			DEPENDENTS [ DEPENDENTS.length - 1 ]
		);
	}

	return getModules ({
		NAME,
		VERSION,

		DEPENDENTS: require ("lodash/cloneDeep") (DEPENDENTS),
		DEPENDENCIES: Dependencies
	});
}

module.exports = {
	getModules,
	getModule
}
