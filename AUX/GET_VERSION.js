
/*
	var { ALREADY_INSTALLED, VERSION } = await require ("./AUX/GET_VERSION") ({
		NAME,
		SPECIFIER,
		Versions
	});
*/

/*
	A satifactory version is already installed??

		e.g.
			A/0.0.1
				B: ^2.0.0

			Y/0.0.1
				B: ^2.1.0

			///// Resolution ///////////////
			A -> B/2.3.4
			Z -[ Use ] -> B/2.3.4


	A satifactory version isn't already installed
		// e.g.
		// 	A/0.0.1
		// 		B: ^2.0.0
		//
		// 	Y/0.0.1
		// 		B: ^3.1.0
		//
		// 	A -> B/2.3.4
		// 	Z -[ Use ] -> B/3.1.0
*/

module.exports = function ({
	NAME,
	SPECIFIER,
	Versions
}) {
	return new Promise (async (Resolve) => {

		/*
			Is already installed????

				This should instead check based on the file system.....
					or it could check based on the INSTALLATIONS.json file.....
						but that isn't really reliable???????????
		*/

		// console.log ("INSTALLATIONS", NAME, INSTALLATIONS [ NAME ]);




		if (typeof INSTALLATIONS [ NAME ] === "object") {
			const MODULE_INSTALLATIONS = INSTALLATIONS [ NAME ];
			for (const INSTALLATION_VERSION in MODULE_INSTALLATIONS) {
				const Satisfactory = require ('semver/functions/satisfies') (
					INSTALLATION_VERSION,
					SPECIFIER
				);

				if (Satisfactory) {
					Resolve ({
						ALREADY_INSTALLED: true,
						VERSION: INSTALLATION_VERSION
					});
					return;
				}
			}
		}

		/*
			Isn't already installed, find highest (????? but not beta???) version that satisfies semver
		*/
		for (let A = Versions.length - 1; A >= 0; A--) {
			const Version = Versions [ A ].Version;

			const Satisfactory = require ('semver/functions/satisfies') (Version, SPECIFIER);

			if (Satisfactory) {
				Resolve ({
					ALREADY_INSTALLED: false,
					VERSION: Version
				});
				return;
			}
		}

		console.error (`A matching version of "${ NAME }" could not found.`, {
			SPECIFIER,
			Versions
		});
	});
}
