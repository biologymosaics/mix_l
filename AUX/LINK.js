
var HERE = require ("./SETTINGS").HERE;
var MODULES = require ("./SETTINGS").MODULES;

module.exports = async function ({
	NAME,
	VERSION,
	MODULES,
	HERE,
	DEPENDENTS,
	isOrg = false
}) {
	var TO = require ("path").resolve (MODULES, NAME + "/" + VERSION);
	var FROM = require ("path").resolve (HERE, "node_modules", NAME);

	var _FROM = require ("path").resolve (HERE, "node_modules");

	if (DEPENDENTS.length >= 1) {
		_FROM += "/[MODULES]";

		var DEPENDENT = DEPENDENTS [ DEPENDENTS.length - 1 ];
		await new Promise (Resolve => {
			_FROM += `/${ DEPENDENT.NAME }/${ DEPENDENT.VERSION }/node_modules`
			require ("fs").mkdir (_FROM, {
				recursive: true
			}, (Throw) => {
				if (Throw) {
					console.error (Throw);
					process.exit (2);
				}

				// console.log ("MADE", _FROM);

				Resolve ();
			});
		});
	}
	_FROM += "/" + NAME;

	if (isOrg) {
		// console.log ("ORG MODULE???")

		const ORG_LOCATION = require ("path").dirname (_FROM);

		/*
			If an organizational module???
		*/
		await new Promise (Resolve => {
			// console.log ("MAKING", ORG_LOCATION)

			require ("fs").mkdir (ORG_LOCATION, {
				recursive: true
			}, (Throw) => {
				if (Throw) {
					console.error (Throw);
					process.exit (2);
				}

				Resolve ();
			});
		});

		/*
			If an organizational module???
		*/
		await new Promise (Resolve => {
			require ("fs").mkdir (TO, {
				recursive: true
			}, (Throw) => {
				if (Throw) {
					console.error (Throw);
					process.exit (2);
				}

				Resolve ();
			});
		});
	}


	var _TO = require ("path").relative (
		require ("path").dirname (_FROM),
		TO
	);

	// console.log ({ _FROM, FROM, TO, _TO });
	// console.log ({ _FROM, _TO, TO });

	// return;

	// node_modules/express -> [MODULES]/express/4.17.1

	// [MODULES]/iconv-lite/0.4.24/node_modules/safer-buffer -> ../../../safer-buffer/2.1.2

	var __FROM = require ("path").relative (HERE, _FROM);

	return new Promise (Resolve => {
		require ("fs").symlink (_TO, _FROM, (Throw) => {
			if (Throw) {
				console.error (Throw);
				process.exit (2);
			}

			console.log ("\\_ Linked:", __FROM, "->", _TO);

			Resolve ();
		})
	});
}
