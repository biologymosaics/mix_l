

(async () => {
	await require ("./../REFRESH") ();

	require ("./../INSTALL") ({
		DEPENDENCIES: [
			[ "chalk", "*", "NPM" ],

			// [ "express", "*", "NPM" ],
			// [ "@mixes/a", "*", "NPM" ]
		]
	});

	// require ("./../DOWNLOAD") ({
	// 	DEPENDENCIES: [
	// 		[ "chalk" ]
	// 	]
	// });
}) ();
