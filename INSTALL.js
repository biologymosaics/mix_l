





require ("./AUX/EVENTS") ();

var HERE = require ("./AUX/SETTINGS").HERE;
var MODULES = require ("./AUX/SETTINGS").MODULES;

var OUTPUT = require ("path").resolve (
	HERE,
	"INSTALLATIONS.json"
);

global.INSTALLATIONS = {};

var { getModule, getModules } = require ("./AUX/GET_MODULES");

module.exports = function ({
	DEPENDENCIES
}) {
	return new Promise (async (Resolve) => {
		var _DEPENDENCIES = DEPENDENCIES.map (D => {
			return {
				Name: D [ 0 ],
				VERSION: D [ 1 ] || null
			}
		});

		await getModules ({
			DEPENDENCIES: _DEPENDENCIES
		});

		var Result = require ("./AUX/SORT") ();

		require ("fs").writeFile (OUTPUT, JSON.stringify (Result, null, 2), (Throw) => {
			if (Throw) {
				console.error (Throw);
				process.exit (2);
			}

			console.log ("Concluded");
		});
	});
}







///////
