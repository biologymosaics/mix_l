
var HERE = require ("./AUX/SETTINGS").HERE;

module.exports = async function () {
	return await require ("./AUX/REFRESH") ({
		HERE
	});
}
